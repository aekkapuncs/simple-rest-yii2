<?
use yii\db\Schema;
use yii\db\Migration;

class m150713_073501_createCatalogTable extends Migration{
	public function up(){
		$tableOptions = NULL;
		if( $this->db->driverName === 'mysql' ){
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable( '{{%item}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL',
			'price' => Schema::TYPE_DOUBLE . ' NOT NULL',
			'description' => Schema::TYPE_STRING . ' NULL',
			'img' => Schema::TYPE_STRING . ' NULL',
			'category_id' => Schema::TYPE_INTEGER . ' NULL'
		], $tableOptions );

		$this->createTable( '{{%category}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL',
			'code' => Schema::TYPE_STRING . ' NOT NULL'
		], $tableOptions );

		$this->createTable( '{{%color}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL'
		], $tableOptions );

		$this->createTable( '{{%item_color}}', [
			'id' => Schema::TYPE_PK,
			'item_id' => Schema::TYPE_INTEGER,
			'color_id' => Schema::TYPE_INTEGER
		], $tableOptions );

		$this->createIndex( 'IX_catalog_item', '{{%item}}', 'category_id' );
		$this->addForeignKey( 'FK_catalog_item', '{{%item}}', 'category_id', '{{%category}}', 'id', 'SET NULL', 'CASCADE' );

		$this->createIndex( 'IX_item_color_item', '{{%item_color}}', 'item_id' );
		$this->addForeignKey( 'FK_item_color_item', '{{%item_color}}', 'item_id', '{{%item}}', 'id', 'CASCADE', 'CASCADE' );

		$this->createIndex( 'IX_item_color_color', '{{%item_color}}', 'color_id' );
		$this->addForeignKey( 'FK_item_color_color', '{{%item_color}}', 'color_id', '{{%color}}', 'id', 'CASCADE', 'CASCADE' );
	}

	public function down(){
		$this->dropTable( '{{%item_color}}' );
		$this->dropTable( '{{%item}}' );
		$this->dropTable( '{{%color}}' );

		$this->dropTable( '{{%category}}' );
	}
}
