<?
namespace app\modules\rest\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use app\modules\user\models\User;
use app\modules\rest\models\Category;
use app\modules\rest\models\Order;
use app\modules\rest\models\ItemToOrder;

class DefaultController extends Controller{
	public function beforeAction( $action ){
		// ...set `$this->enableCsrfValidation` here based on some conditions...
		// call parent method that will check CSRF if such property is true.
		$this->enableCsrfValidation = false;

		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		$auth = false;
		if( array_key_exists( 'key', $_GET ) ){
			$model = new User;
			$user = $model->findByAuthKey( htmlspecialchars( $_GET['key'] ) );
			if( $user !== NULL ){
				$auth = true;
			}
		}

		if( !$auth ){
			Yii::warning( 'unauthorized access to action: '.$action->id );
			throw new HttpException( 403, 'access denied' );
		}

		return parent::beforeAction( $action );
	}

	public function actionIndex(){
		$res = [];

		$categories = Category::find()->all();
		foreach( $categories as $category ){
			$items = [];
			foreach( $category->items as $item ){
				$colors = [];

				foreach( $item->colors as $color ){
					$colors[$color->id] = $color->name;
				}

				$items[$item->id] = [
					'id' => $item->id,
					'name' => $item->name,
					'price' => $item->price,
					'colors' => $colors,
					'description' => $item->description,
					'img' => $item->img
				];
			}

			$res[$category['code']] = $items;
		}

		$res['min_price'] = 500; // TODO: mv to project db settings

		return $res;
	}

	public function actionGet(){
		return Yii::$app->session->get( 'order' ) ?: [];
	}

	public function actionSave(){
		$status = '';
		$message = '';
		$total_price = 0;

		$post = \Yii::$app->request->post();
		if( $post ){
			$res = [
				'items' => [],
				'total_price' => 0
			];

			if( array_key_exists( 'items', $post ) ){
				foreach( $post['items'] as $item ){
					$item = (array)$item;

					if( !array_key_exists( 'id', $item ) || !$item['id'] ){
						$status = 'ERROR';
						$message[] = 'not found id in item post';
						break;
					}

					if( !array_key_exists( 'price', $item ) || !$item['price'] ){
						$status = 'ERROR';
						$message[] = 'not found price in item post';
						break;
					}

					if( !array_key_exists( 'count', $item ) || !$item['count'] ){
						$status = 'ERROR';
						$message[] = 'not found count in item post';
						break;
					}

					$res[] = [
						'id' => $item['id'],
						'color' => $item['color'] ?: NULL,
						'price' => $item['price'],
						'count' => $item['count']
					];

					$total_price += $item['price'] * $item['count'];
				}
			}else{
				$status = 'ERROR';
				$message[] = 'empty items';
			}

			if( !$status ){
				$res['total_price'] = $total_price;

				Yii::$app->session['order'] = $res;
				$status = 'OK';
			}
		}else{
			$status = 'ERROR';
			$message[] = 'empty data';
		}

		Yii::warning( 'PUT: '.json_encode( [
				'STATUS' => $status,
				'message' => $message,
				'total_price' => $total_price
			] ) );

		return [
			'STATUS' => $status,
			'message' => $message,
			'total_price' => $total_price
		];
	}

	public function actionDelete(){
		$message = '';

		Yii::$app->session['order'] = [];

		if( Yii::$app->session->get( 'order' ) ){
			$status = 'ERROR';
			$message = 'error delete session';
		}else{
			$status = 'OK';
		}

		Yii::warning( 'DELETE: '.json_encode( [
				'STATUS' => $status,
				'message' => $message
			] ) );

		return [
			'STATUS' => $status,
			'message' => $message
		];
	}

	public function actionLoad(){
		$status = '';
		$message = '';
		$total_price = 0;

		if( Yii::$app->session->get( 'order' ) ){
			$order = (array)Yii::$app->session->get( 'order' );

			$ids = [];

			foreach( $order['items'] as $item ){
				$model = new ItemToOrder;
				$model->item_id = $item['id'];
				$model->price = $item['price'];
				$model->color_id = $item['color'];
				$model->count = $item['count'];

				if( $model->validate() ){
					if( $model->save() ){
						$ids[] = $model->id;
					}else{
						$status = 'ERROR';
						$message = 'error save item';
						break;
					}
				}else{
					$status = 'ERROR';
					$message = 'error load item';
					break;
				}
			}

			if( !$status ){
				$model = new Order;
				$model->items = $ids;
				$model->total_price = $order['total_price'];
				if( $model->validate() ){
					if( $model->save() ){
						$status = 'OK';
					}else{
						$status = 'ERROR';
						$message = 'error save order';
					}
				}else{
					$status = 'ERROR';
					$message = 'error load order';
				}
			}
		}else{
			$status = 'ERROR';
			$message = 'empty data';
		}

		Yii::warning( 'POST: '.json_encode( [
				'STATUS' => $status,
				'message' => $message,
				'total_price' => $total_price,
				'redirect_url' => '/basket'
			] ) );

		return [
			'STATUS' => $status,
			'message' => $message,
			'total_price' => $total_price,
			'redirect_url' => '/basket'
		];
	}
}
