<?
namespace app\modules\rest\models;

use Yii;

class Color extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'color';
	}

	public function rules(){
		return [
			[
				[ 'name' ],
				'required'
			],
			[
				[ 'name' ],
				'string',
				'max' => 255
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Название'
		];
	}

	public function getItems(){
		return $this->hasMany( Color::className(), [ 'id' => 'item_id' ] )
			->viaTable( 'item_color', [
				'color_id' => 'id'
			] );
	}
}
