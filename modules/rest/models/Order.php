<?
namespace app\modules\rest\models;

use Yii;
use voskobovich\behaviors\ManyToManyBehavior;

class Order extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'order';
	}

	public function rules(){
		return [
			[
				[ 'price' ],
				'required'
			],
			[
				[ 'price' ],
				'number'
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'price' => 'Цена'
		];
	}

	public function getAttributes( $names = null, $except = [ ] ){
		$attributes = parent::getAttributes( $names = null, $except = [ ] );

		return array_replace( $attributes, [
			'items' => $this->items
		] );
	}

	public function behaviors(){
		return [
			[
				'class' => ManyToManyBehavior::className(),
				'relations' => [
					'items' => 'items'
				]
			]
		];
	}

	public function getColors(){
		return $this->hasMany( ItemToOrder::className(), [ 'id' => 'item_id' ] )
			->viaTable( 'order_item', [
				'order_id' => 'id'
			] );
	}
}
