<?
namespace app\modules\user\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\modules\user\models\LoginForm;
use app\modules\user\models\ConfirmEmailForm;
use app\modules\user\models\PasswordResetRequestForm;
use app\modules\user\models\ResetPasswordForm;
use app\modules\user\models\SignupForm;

class DefaultController extends Controller{
	public function behaviors(){
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => [ 'logout' ],
				'rules' => [
					[
						'actions' => [ 'logout' ],
						'allow' => true,
						'roles' => [ '@' ]
					]
				]
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => [ 'post' ]
				]
			]
		];
	}

	public function actions(){
		return [
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
			]
		];
	}

	public function actionLogin(){
		if( !\Yii::$app->user->isGuest ){
			return $this->goHome();
		}

		$model = new LoginForm();
		if( $model->load( Yii::$app->request->post() ) && $model->login() ){
			return $this->goBack();
		}else{
			return $this->render( 'login', [
				'model' => $model,
			] );
		}
	}

	public function actionLogout(){
		Yii::$app->user->logout();

		return $this->goHome();
	}

	public function actionSignup(){
		$model = new SignupForm();
		if( $model->load( Yii::$app->request->post() ) ){
			if( $user = $model->signup() ){
				Yii::$app->getSession()
					->setFlash( 'success', 'Подтвердите ваш электронный адрес.' );

				return $this->goHome();
			}
		}

		return $this->render( 'signup', [
			'model' => $model,
		] );
	}

	public function actionConfirmEmail( $token = '' ){
		$model = new ConfirmEmailForm( $token );

		$errors = $model->errors;

		if( !$errors ){
			if( $model->confirmEmail() ){
				Yii::$app->getSession()
					->setFlash( 'success', 'Спасибо! Ваш Email успешно подтверждён.' );
			}else{
				Yii::$app->getSession()
					->setFlash( 'error', 'Ошибка подтверждения Email.' );
			}
		}else{
			Yii::$app->getSession()
				->setFlash( 'error', current( $errors )[0] );
		}

		return $this->goHome();
	}

	public function actionRequestPasswordReset(){
		$model = new PasswordResetRequestForm();
		if( $model->load( Yii::$app->request->post() ) && $model->validate() ){
			if( $model->sendEmail() ){
				Yii::$app->getSession()
					->setFlash( 'success', 'Инструкция по восстановлению пароля отправлена вам на e-mail.' );

			}else{
				Yii::$app->getSession()
					->setFlash( 'error', 'Произошла ошибка, воспользуйтесь сервисом позже.' );
			}

			return $this->goHome();
		}

		return $this->render( 'requestPasswordResetToken', [
			'model' => $model
		] );
	}

	public function actionResetPassword( $token = '' ){
		$model = new ResetPasswordForm( $token );

		$errors = $model->errors;

		if( !$errors ){
			if( $model->load( Yii::$app->request->post() ) && $model->validate() ){
				if( $model->resetPassword() ){
					Yii::$app->getSession()
						->setFlash( 'success', 'Новый пароль установлен.' );
				}else{
					Yii::$app->getSession()
						->setFlash( 'error', 'Ошибка установки пароля.' );
				}

				return $this->goHome();
			}
		}else{
			Yii::$app->getSession()
				->setFlash( 'error', current( $errors )[0] );

			return $this->goHome();
		}

		return $this->render( 'resetPassword', [
			'model' => $model
		] );
	}
}
