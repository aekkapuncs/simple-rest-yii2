<?
namespace app\modules\user\models;

use Yii;
use yii\base\Model;

class ConfirmEmailForm extends Model{
	private $_user;

	public function __construct( $token, $config = [ ] ){
		if( empty( $token ) || !is_string( $token ) ){
			$this->addError( 'notoken', 'Отсутствует код подтверждения.' );
		}

		$this->_user = User::findByEmailConfirmToken( $token );
		if( !$this->_user ){
			$this->addError( 'incorrecttoken', 'Неверный токен.' );
		}

		if( !$this->errors ){
			parent::__construct( $config );
		}
	}

	public function confirmEmail(){
		$user = $this->_user;
		$user->status = User::STATUS_ACTIVE;
		$user->removeEmailConfirmToken();

		return $user->save();
	}
}