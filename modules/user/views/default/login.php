<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
	<h1><?= Html::encode( $this->title ) ?></h1>

	<p>Для авторизация заполните форму ниже:</p>

	<div class="row">
		<div class="col-lg-5">
			<? $form = ActiveForm::begin( [ 'id' => 'login-form' ] ); ?>

			<?= $form->field( $model, 'username' ) ?>

			<?= $form->field( $model, 'password' )
				->passwordInput() ?>

			<?= $form->field( $model, 'rememberMe', [
				'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
			] )
				->checkbox() ?>

			<div style="color:#999;margin:1em 0">
				Если вы забыли пароль, вы можете <?= Html::a( 'восстановить его', [ '/user/default/request-password-reset' ] )
				?>.
			</div>

			<div class="form-group">
				<?= Html::submitButton( 'Войти', [
					'class' => 'btn btn-primary',
					'name' => 'login-button'
				] ) ?>
			</div>
		</div>
	</div>

	<? ActiveForm::end() ?>
</div>
