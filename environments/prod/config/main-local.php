<?
$db = require( __DIR__ . '/db.php' );

return [
	'components' => [
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => ''
		],
		'db' => $db,
		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'useFileTransport' => false
		]
	]
];
